all:
	(cd journal/TeX;make all)
	(cd symposium/TeX;make all)

clean:
	(cd journal/TeX;make clean)
	(cd symposium/TeX;make clean)

distclean:
	(cd journal/TeX;make distclean)
	(cd symposium/TeX;make distclean)
